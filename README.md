# opensuse-arm


# Download

http://mirror.easyname.at/opensuse/distribution/leap/15.3/appliances/openSUSE-Leap-15.3-ARM-JeOS-raspberrypi.aarch64.raw.xz

http://mirror.easyname.at/opensuse/distribution/leap/15.4/appliances/openSUSE-Leap-15.4-ARM-JeOS-raspberrypi.aarch64.raw.xz


 https://get.opensuse.org/leap/15.4/

http://mirror.easyname.at/opensuse/distribution/leap/15.4/



# Help 

HCL:Raspberry Pi3

Raspberry Pi 3
The Raspberry Pi 3 is a credit-card sized computer that plugs into your TV and a keyboard. It’s a capable little PC which can be used for many of the things that your desktop PC does, like spreadsheets, word-processing and games. It also plays high-definition video.

Technical Data
Broadcom BCM2837 SoC
4x ARM Cortex-A53 CPU
1GB RAM
40-pin connector also see Using The Header
openSUSE in Raspberry Pi 3
There are three ways:

Writing a disk image to an SD card (easiest)
or, installing openSUSE using standard DVD (advanced)
or, emulating openSUSE using QEMU (experimental, advanced)
Writing a disk image to a USB stick or a SD card (easiest)
Download the image you want (Leap is stable, Tumbleweed is rolling) from here. Make a choice of desktops:
JeOS - Just enough Operating System - a very basic system, no graphical desktop
E20 - Enlightenment desktop
XFCE - XFCE desktop
KDE - KDE desktop
LXQT - LXQT desktop
X11 - basic X11 system
Geeko-white.png
Tumbleweed
JeOS image E20 image XFCE image LXQT image GNOME image KDE image X11 image
If the direct links above do not work for you, please check the general download directory for the images.


Geeko-white.png
Leap-15.3
JeOS image E20 image XFCE image LXQT image GNOME image KDE image X11 image
If the direct links above do not work for you, please check the general download directory for the images.

As root extract the image onto your SD card (replace sdX with the device name of your SD card).
WARNING: all previous data on the SD card will be lost. Check first if the device you have selected is really your SD card!
 xzcat [image].raw.xz | dd bs=4M of=/dev/sdX iflag=fullblock oflag=direct status=progress; sync
Insert the SD card with the openSUSE image into your board.
Connect the board to your PC via serial port (optional, but helpful for debugging; USB-TTL serial cable needed).
Connect the board to your monitor (via DVI/HDMI, optional).
Power on the board.
Walk through the first boot steps.
Ethernet is configured to request an IP via DHCP, check your DHCP server for the board IP if used.
Have a lot of fun...
Default login is root:linux, works on serial console, via ssh, GUI.
installing openSUSE using standard ISO (advanced)
It is possible to directly install from the DVD ISO, or the NET ISO, on Raspberry Pi 3.

If you use a version with only 1 GB RAM, do not use the NET ISO and do not activate online repositories during installation with DVD ISO.

To boot the DVD there are multiple ways:

copying the ISO on a USB key
booting the ISO through the network boot (PXE)
USB key installation method
A USB key (that can contains the DVD) and an empty SD card (at least 16GB) are needed.

The ISO (DVD and NET images) are able to boot on the Raspberry Pi. So, you just need to copy the image to an USB stick, plug it in the RPi and follow instructions from HCL:AArch64_EFI

Network/PXE installation method
TODO
Known issues
DSI output not supported by VC4 driver
DSI output used for some LCD panels (using the flat cable) is not yet supported by the VC4 driver. 
To use this video output, please use the EFI framebuffer by replacing:

dtoverlay=vc4-kms-v3d,cma-default
with:

dtoverlay=disable-vc4
in /boot/efi/config.txt

More details on boo#1181683 and https://github.com/raspberrypi/linux/issues/4020

Troubleshooting
I don't see kernel output on serial or HDMI
If you don't see kernel output on serial or HDMI, you can try adding earlycon=uart8250,mmio32,0x3f215040 to the kernel command line.

I see HDMI output in U-Boot, but not in Linux
The upstream Linux graphics driver for the Raspberry Pi has problems with a few monitors. The same applies to the 7" LCD displays. In those cases, please fall back to the efi frame buffer console by passing the following into the kernel command line:

 modprobe.blacklist=vc4

I cannot use keyboard in U-Boot and Grub but it works in Linux
USB 1.1 keyboards don't work well with the dwc2 USB interface. Get an USB2 keyboard or upgrade u-boot to a version that has fix for bug 1139351. Even with the fix you must press a key when "USB0: scanning bus 0 for devices..." is printed. Otherwise you will see a probe error for the keyboard and it will not be enabled in u-boot and grub.

Boot from USB
If you are using a Raspberry Pi 3 A+ or B+, you can boot from USB directly.

If you are using another Raspberry Pi 3, you can enable USB boot by updating OTP memory. For that, you need to boot once from an SD card with a Config.txt file on 1st partition (along the Raspberry Pi firmware) which include the following line:

 program_usb_boot_mode=1
If you want to the installer to put the output to the serial console instead of HDMI, add the following kernel boot parameter in the Grub menu:

 console=ttyS0,115200
Once done, your Raspberry Pi 3 will try to start from USB, if no SD card.

More information on: https://www.raspberrypi.org/documentation/hardware/raspberrypi/bootmodes/msd.md

Graphics Acceleration
The package xf86-video-fbturbo offers a drop-in replacement driver for Hardware accelerated window moving/scrolling on boards with BCM2835 DMA Controller. In order to load the driver, add a section

Section "Module"
        Load    "shadow"
EndSection
to /etc/X11/xorg.conf.d/99-fbturbo.conf and restart the X-Server. (See boo#1163230)

For 3D acceleration, install the package Mesa-dri-vc4. In the file /etc/X11/Xorg.conf.d/20-kms.conf, remove the line 
Option "AccelMethod" "none" 
After a restart of the X-Server the windows now move smoothly over the desktop
